package com.hr.oauth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hr.oauth.feignclients.UserFeignClient;
import com.hr.oauth.model.User;

@Service
public class UserService implements UserDetailsService {

	private static Logger log = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserFeignClient userFeignClient;

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			User user = userFeignClient.findByEmail(username);
			log.info("User found");
			return user;
		} catch (Exception e) {
			log.error("User not found with this e-mail {}", username);
			throw new UsernameNotFoundException("E-mail not found");
		}
	}

}
