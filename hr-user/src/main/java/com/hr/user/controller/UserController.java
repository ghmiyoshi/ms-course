package com.hr.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hr.user.model.User;
import com.hr.user.repository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository repository;

	@GetMapping("/{id}")
	public User findById(@PathVariable Long id) {
		return repository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
	}

	@GetMapping("/search")
	public User findByEmail(@RequestParam String email) {
		return repository.findByEmail(email).orElseThrow(() -> new RuntimeException("User not found with this email"));
	}

}
