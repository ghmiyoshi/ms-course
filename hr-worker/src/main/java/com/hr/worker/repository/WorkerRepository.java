package com.hr.worker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hr.worker.entity.Worker;

@Repository
public interface WorkerRepository extends JpaRepository<Worker, Long> {

}
