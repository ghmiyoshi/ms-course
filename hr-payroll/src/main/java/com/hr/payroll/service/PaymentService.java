package com.hr.payroll.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.payroll.feignclients.WorkerFeignClient;
import com.hr.payroll.model.Payment;
import com.hr.payroll.model.Worker;

@Service
public class PaymentService {

	@Autowired
	private WorkerFeignClient workerFeignClient;

	public Payment getPayment(Long workerId, Integer days) {
		Worker worker = workerFeignClient.findById(workerId);
		return new Payment(worker.getName(), worker.getDailyIncome(), days);
	}

}
