package com.hr.payroll.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hr.payroll.model.Payment;
import com.hr.payroll.service.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
@RequestMapping("/payments")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@HystrixCommand(fallbackMethod = "getPaymentAlternative")
	@GetMapping("/{workedId}/days/{days}")
	public Payment getPayment(@PathVariable("workedId") Long id, @PathVariable Integer days) {
		return paymentService.getPayment(id, days);
	}
	
	public Payment getPaymentAlternative(Long id, Integer days) {
		return new Payment("Brann", new BigDecimal("400.0"), days);
	}
	
}
