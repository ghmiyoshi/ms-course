package com.hr.payroll.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonGetter;

public class Payment {

	private String name;
	private BigDecimal dailyIncome;
	private Integer days;

	public Payment(String name, BigDecimal dailyIncome, Integer days) {
		this.name = name;
		this.dailyIncome = dailyIncome;
		this.days = days;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getDailyIncome() {
		return dailyIncome;
	}

	public Integer getDays() {
		return days;
	}

	@JsonGetter("total")
	public BigDecimal calcularTotal() {
		return dailyIncome.multiply(new BigDecimal(days));
	}

}
